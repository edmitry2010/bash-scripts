#!/usr/bin/bash

DirCert='/var/lib/samba/private/tls'
NameDC='dc1'
NameDom='dima.home'
Date=`date +%Y.%m.%d`
Time=`date +%H.%M.%S`

VersOC=`cat /etc/*rel* | grep '^ID=' | sed 's|ID=||g;s|"||g'`

if ! [[ `openssl version | awk '{print $1}'` == OpenSSL ]]; then
  case $VersOC in
    ubuntu|debian) apt update && apt -y install openssl;;
           fedora) dnf -y install openssl;;
  esac
fi

function TestSSL() {
  local per=($*)
  # openssl x509 -noout -text -in /var/lib/samba/private/tls/ca.pem | grep 'Not After' | awk '{print $4,$5,$6,$7,$8}'

  if [[ `ls $per 2> /dev/null` == $per ]]; then
    local SSLtest=`openssl x509 -noout -text -in $per | grep 'Not After' | awk '{print $4,$5,$6,$7,$8}'`
    echo "Сертификат: $per -> $SSLtest"
  else
    echo 'Нет ни файла ни ссылки, продолжаем'
  fi
}
TestSSL $DirCert/ca.pem
TestSSL $DirCert/cert.pem

read -r -p "Продолжаем, создать новые сертификаты? (yes|y), пропуск выход: " SpeedJobe
case $SpeedJobe in
  yes|y) echo 'Продолжаем';;
      *) echo 'Выходим'
         exit 0;;
esac

function TestFileOC() {
  local per=($*)
  if [ -f $per ]; then
    mv $per $per.cop.$Date-$Time
  fi
}
TestFileOC $DirCert/ca.pem
TestFileOC $DirCert/cert.pem
TestFileOC $DirCert/key.pem

function TestDirTLS() {
  local per=($*)
  if ! [ -d $per ]; then
    mkdir -p $per
  fi
}
TestDirTLS $DirCert/$Date-$Time

function TestLnFile() {
  local per=($*)
  if [ -h $per ]; then
    echo ls -al $per
  fi
}
TestLnFile $DirCert/ca.pem
TestLnFile $DirCert/cert.pem
TestLnFile $DirCert/key.pem

function GenSSL() {
  openssl genrsa -out $DirCert/$Date-$Time/ca.key 4096
  openssl req -new -x509 -nodes -days 3650 -key $DirCert/$Date-$Time/ca.key -out $DirCert/$Date-$Time/ca.pem -subj "/O=Home Inc/OU=Samba CA cert/CN=$NameDC.$NameDom"
  openssl genrsa -out $DirCert/$Date-$Time/$NameDC.$NameDom.key 2048
  openssl req -new -sha256 -key $DirCert/$Date-$Time/$NameDC.$NameDom.key -subj "/O=Home inc/OU=Samba AD cert/CN=$NameDC.$NameDom" -out $DirCert/$Date-$Time/$NameDC.$NameDom.csr
  openssl x509 -req -in $DirCert/$Date-$Time/$NameDC.$NameDom.csr -CA $DirCert/$Date-$Time/ca.pem -CAkey $DirCert/$Date-$Time/ca.key -CAcreateserial -out $DirCert/$Date-$Time/$NameDC.$NameDom.pem -days 3650

  chmod 600 $DirCert/$Date-$Time/$NameDC.$NameDom.key
  chmod 644 $DirCert/$Date-$Time/$NameDC.$NameDom.pem
  chmod 644 $DirCert/$Date-$Time/ca.pem

  ln -s $DirCert/$Date-$Time/$NameDC.$NameDom.key $DirCert/key.pem
  ln -s $DirCert/$Date-$Time/$NameDC.$NameDom.pem $DirCert/cert.pem
  ln -s $DirCert/$Date-$Time/ca.pem $DirCert/ca.pem

  echo; echo 'Тест'
  openssl verify -CAfile $DirCert/ca.pem $DirCert/cert.pem
}
GenSSL

echo; echo 'Добавьте в конфиг строки'

echo 'tls enabled  = yes
tls keyfile  = /var/lib/samba/private/tls/key.pem
tls certfile = /var/lib/samba/private/tls/cert.pem
tls cafile   = /var/lib/samba/private/tls/ca.pem
tls priority = NORMAL:-VERS-TLS1.0:-VERS-TLS1.1:-VERS-SSL3.0'
