#!/bin/bash

file_bashrc_user="/$USER/.bashrc"
file_bashrc_sys=`find /etc -name 'bash*rc' | grep --color=no '/etc'`

#echo $file_bashrc_user
#echo $file_bashrc_sys

echo 'Есть пара вариантов прописать настройки'
echo '1. Для пользователя'
echo '2. Для всей системы'
echo 'К сожалению, почему-то не во всех системах работает второй вариант'
echo 'Попробуйте сначала указать второй вариант и если не будет работать, тогда потом выполните под пользователем'
echo
PS3='Цифра из списка: '
select NumListVariant in $(echo $file_bashrc_user $file_bashrc_sys); do
  file_out=$NumListVariant
  break
done

function PrescribeSettings() {
  local per=($*)
  cat >> $per <<EOL

# Это нужно для сохранения истории
HISTCONTROL=ignoredups:erasedups
shopt -s histappend
PROMPT_COMMAND="\${PROMPT_COMMAND:+\$PROMPT_COMMAND\$'\n'}history -a; history -c; history -r"

# Это пошло украшательство
HOSTNAME="\`hostname\`"

INPUT_COLOR="\[\033[2;0m\]"
DIR_COLOR="\[\033[0;33m\]"
DIR="\w"

LINE_VERTICAL="\342\224\200"
LINE_CORNER_1="\342\224\214"
LINE_CORNER_2="\342\224\224"
LINE_CORNER_3="\342\224\200"
LINE_COLOR="\[\033[1;32m\]"

USER_NAME="\[\e[1;36m\]\u\[\e[m\]"
#SYMBOL="\[\033[2;32m\]$"
SYMBOL="$"
SIGNED="\[\033[1;34m\]"

if [[ \${EUID} == 0 ]]; then
    USER_NAME="\[\e[1;31m\]\u\[\e[m\]"
    #SYMBOL="\[\033[2;32m\]#"
    SYMBOL="\[\e[1;31m\]#"
fi

alias grep='grep --color=always'
alias ls='ls --color=always'
alias showextip='curl http://ipecho.net/plain; echo'
alias showcountry='read -r -p "Укажите адрес или оставьте пустым для текущего: " TekAddr; curl ipinfo.io/\$TekAddr; unset TekAddr'
alias md='mkdir -p'
alias mtr='mtr -o "LRSD NBAWV"'
alias dmesg='dmesg --color=always'
alias gcc='gcc -fdiagnostics-color=always'
alias gitl='git log --graph --decorate --all --oneline'
alias dir='dir --color=always'
alias diff='diff --color=always'
alias nan='nano -c -K -H -Y sh'
alias rcp='rsync -avP'
alias rcpd='rsync -avP --delete-excluded'
alias routefl='ip route flush cache'

# tcpdump
alias tcpd='tcpdump -i'
alias tcpdn='tcpdump -nn -i'
alias tcpd6='tcpdump -vv ip6 -i'
alias tcpdn6='tcpdump -nnvv ip6 -i'

# systemd
alias sys='systemctl'
alias sysg='systemctl list-unit-files | grep'
alias sysm='systemctl list-unit-files --type=service --state=masked'
alias syse='systemctl enable'
alias sysd='systemctl disable'
alias sysr='systemctl restart'
alias sysrd='systemctl daemon-reload'

STR_Topline="(\$DIR_COLOR\t\$LINE_COLOR)\$LINE_CORNER_3\$LINE_COLOR(\$DIR_COLOR\$HOSTNAME\$LINE_COLOR@"\$SIGNED"\$USER_NAME\$LINE_COLOR)\$LINE_CORNER_3\$LINE_COLOR(\$DIR_COLOR\$DIR\$LINE_COLOR)"

PS1="\$LINE_COLOR\$LINE_CORNER_1\$LINE_VERTICAL\$STR_Topline \n\$LINE_COLOR\$LINE_CORNER_2\$LINE_VERTICAL\>\$SYMBOL \$INPUT_COLOR"

export PS1="\$PS1\[\e]1337;CurrentDir="'\$(pwd)\a\]'

EOL

}

if [[ `cat $file_out | grep --color=no '^HOSTNAME' | awk -F'=' '{print $1}'` == HOSTNAME ]]; then
  echo
  echo 'HOSTNAME - Уже прописан, очень похоже на то, что вы уже прописали настройки'
  echo 'Попробуйте перелогиниться на сервере, если не поможет, выполните этот скрипт,'
  echo 'под тем пользователем для которого хотите применить настройки и выберите конфиг пользователя.'
else
  echo 'Можно заполнить'
  PrescribeSettings $file_out
fi

echo
echo 'Перелогиньтесь в консоли, для применения настроек.'

